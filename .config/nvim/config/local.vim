colorscheme monokai

let &colorcolumn='80,'.join(range(120,999),',')

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'
let g:node_host_prog = '/home/ben/.nvm/versions/node/v10.14.2/bin/neovim-node-host'

"let g:neomake_css_enabled_makers = ['stylelint']
"let g:neomake_html_enabled_makers = ['htmlhint']
"let g:neomake_javascript_eslint_exe = 'eslint_d'
"let g:neomake_sass_enabled_makers = ['stylelint']
"let g:neomake_scss_enabled_makers = ['stylelint']
let g:deoplete#num_processes = 1

let g:airline_powerline_fonts = 1
let g:airline_theme = 'base16_monokai'

let g:ackprg = 'ag --nogroup --nocolor --column'
